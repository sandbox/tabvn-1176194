<?php

//$id
function iosdrupal_config_form() {
  $types = _node_types_build()->names;
  $image_style = image_styles();
  $styles = array();
  if (!empty($image_style)) {
    foreach($image_style as $key=>$val){
      $styles[$key] = $key;
    }
  }
  $form['iosdrupal_content_type'] = array(
      '#title' => 'Select content type',
      '#type' => 'select',
      '#options' => $types,
      '#default_value' => variable_get('iosdrupal_content_type', 'article'),
  );

  $form['iosdrupal_key'] = array(
      '#type' => 'textfield',
      '#title' => 'Secure key',
      '#default_value' => variable_get('iosdrupal_key', ''),
  );

  $form['iosdrupal_image_small'] = array(
      '#title' => 'Small image format',
      '#type' => 'select',
      '#options' => $styles,
      '#default_value' => variable_get('iosdrupal_image_small', 'thumbnail'),
  );

   $form['iosdrupal_image_large'] = array(
      '#title' => 'View image format',
      '#type' => 'select',
      '#options' => $styles,
      '#default_value' => variable_get('iosdrupal_image_large', 'large'),
  );

  return system_settings_form($form);
}

?>
