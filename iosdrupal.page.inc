<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function iosdrupal_post() {
  $secure_key = variable_get('iosdrupal_key', '');
  if (isset($_GET['key']) && $_GET['key'] == $secure_key) {
    $iosdrupal_type_setting = variable_get('iosdrupal_content_type', 'article');
    $node = (object) array('uid' => 0, 'type' => $iosdrupal_type_setting, 'language' => LANGUAGE_NONE);
    $node->is_new = TRUE;
    node_object_prepare($node);
    $node->tnid = 0;
    $node->promote = 1;
    $node->comment = 2;
    $node->title = isset($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : 'Untitle';
    if (isset($_GET['title']) && $_GET['title']) {
      $node->title = $_GET['title'];
    }

// node image
    if (isset($_FILES['userfile']['name'])) {
      $filepath = 'public://image/';
      file_prepare_directory($filepath, FILE_CREATE_DIRECTORY);

      $file = iosdrupal_file_save_upload('userfile', array('file_validate_extensions' => array('jpg jpeg gif png')), $filepath);
      if ($file) {
        $file = get_object_vars($file);
      }
      if ($file) {

        $node->field_image = array(
            'und' => array(
                0 => $file,
            )
        );
      }
    }

    node_save($node);




    print '1';
    die();
  } else {
    print '0';
    die();
  }
}

function iosdrupal_file_save_upload($source, $validators = array(), $destination = FALSE, $replace = FILE_EXISTS_RENAME) {
  global $user;
  static $upload_cache;


// Make sure there's an upload to process.
  if (empty($_FILES['userfile']['name'])) {
    return NULL;
  }

// Check for file upload errors and return FALSE if a lower level system
// error occurred. For a complete list of errors:
// See http://php.net/manual/en/features.file-upload.errors.php.
  switch ($_FILES['userfile']['error']) {
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      drupal_set_message(t('The file %file could not be saved, because it exceeds %maxsize, the maximum allowed size for uploads.', array('%file' => $_FILES['userfile']['name'], '%maxsize' => format_size(file_upload_max_size()))), 'error');
      return FALSE;

    case UPLOAD_ERR_PARTIAL:
    case UPLOAD_ERR_NO_FILE:
      drupal_set_message(t('The file %file could not be saved, because the upload did not complete.', array('%file' => $_FILES['userfile']['name'])), 'error');
      return FALSE;

    case UPLOAD_ERR_OK:
// Final check that this is a valid upload, if it isn't, use the
// default error handler.
      if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
        break;
      }

// Unknown error
    default:
      drupal_set_message(t('The file %file could not be saved. An unknown error has occurred.', array('%file' => $_FILES['userfile']['name'])), 'error');
      return FALSE;
  }

// Begin building file object.
  $file = new stdClass();
  $file->uid = $user->uid;
  $file->status = 0;
  $file->filename = trim(basename($_FILES['userfile']['name']), '.');
  $file->uri = $_FILES['userfile']['tmp_name'];
  $file->filemime = file_get_mimetype($file->filename);
  $file->filesize = $_FILES['userfile']['size'];

  $extensions = '';
  if (isset($validators['file_validate_extensions'])) {
    if (isset($validators['file_validate_extensions'][0])) {
// Build the list of non-munged extensions if the caller provided them.
      $extensions = $validators['file_validate_extensions'][0];
    } else {
// If 'file_validate_extensions' is set and the list is empty then the
// caller wants to allow any extension. In this case we have to remove the
// validator or else it will reject all extensions.
      unset($validators['file_validate_extensions']);
    }
  } else {
// No validator was provided, so add one using the default list.
// Build a default non-munged safe list for file_munge_filename().
    $extensions = 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp';
    $validators['file_validate_extensions'] = array();
    $validators['file_validate_extensions'][0] = $extensions;
  }

  if (!empty($extensions)) {
// Munge the filename to protect against possible malicious extension hiding
// within an unknown file type (ie: filename.html.foo).
    $file->filename = file_munge_filename($file->filename, $extensions);
  }

// Rename potentially executable files, to help prevent exploits (i.e. will
// rename filename.php.foo and filename.php to filename.php.foo.txt and
// filename.php.txt, respectively). Don't rename if 'allow_insecure_uploads'
// evaluates to TRUE.
  if (!variable_get('allow_insecure_uploads', 0) && preg_match('/\.(php|pl|py|cgi|asp|js)(\.|$)/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
    $file->filemime = 'text/plain';
    $file->uri .= '.txt';
    $file->filename .= '.txt';
// The .txt extension may not be in the allowed list of extensions. We have
// to add it here or else the file upload will fail.
    if (!empty($extensions)) {
      $validators['file_validate_extensions'][0] .= ' txt';
      drupal_set_message(t('For security reasons, your upload has been renamed to %filename.', array('%filename' => $file->filename)));
    }
  }

// If the destination is not provided, use the temporary directory.
  if (empty($destination)) {
    $destination = 'temporary://';
  }

// Assert that the destination contains a valid stream.
  $destination_scheme = file_uri_scheme($destination);
  if (!$destination_scheme || !file_stream_wrapper_valid_scheme($destination_scheme)) {
    drupal_set_message(t('The file could not be uploaded, because the destination %destination is invalid.', array('%destination' => $destination)), 'error');
    return FALSE;
  }

  $file->source = $source;
// A URI may already have a trailing slash or look like "public://".
  if (substr($destination, -1) != '/') {
    $destination .= '/';
  }
  $file->destination = file_destination($destination . $file->filename, $replace);
// If file_destination() returns FALSE then $replace == FILE_EXISTS_ERROR and
// there's an existing file so we need to bail.
  if ($file->destination === FALSE) {
    drupal_set_message(t('The file %source could not be uploaded because a file by that name already exists in the destination %directory.', array('%source' => $source, '%directory' => $destination)), 'error');
    return FALSE;
  }

// Add in our check of the the file name length.
  $validators['file_validate_name_length'] = array();

// Call the validation functions specified by this function's caller.
  $errors = file_validate($file, $validators);

// Check for errors.
  if (!empty($errors)) {
    $message = t('The specified file %name could not be uploaded.', array('%name' => $file->filename));
    if (count($errors) > 1) {
      $message .= theme('item_list', array('items' => $errors));
    } else {
      $message .= ' ' . array_pop($errors);
    }
    form_set_error($source, $message);
    return FALSE;
  }

// Move uploaded files from PHP's upload_tmp_dir to Drupal's temporary
// directory. This overcomes open_basedir restrictions for future file
// operations.
  $file->uri = $file->destination;
  if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $file->uri)) {
    form_set_error($source, t('File upload error. Could not move uploaded file.'));
    watchdog('file', 'Upload error. Could not move uploaded file %file to destination %destination.', array('%file' => $file->filename, '%destination' => $file->uri));
    return FALSE;
  }

// Set the permissions on the new file.
  drupal_chmod($file->uri);

// If we are replacing an existing file re-use its database record.
  if ($replace == FILE_EXISTS_REPLACE) {
    $existing_files = file_load_multiple(array(), array('uri' => $file->uri));
    if (count($existing_files)) {
      $existing = reset($existing_files);
      $file->fid = $existing->fid;
    }
  }

// If we made it this far it's safe to record this file in the database.
  if ($file = file_save($file)) {
// Add file to the cache.
    $upload_cache[$source] = $file;
    return $file;
  }
  return FALSE;
}

/**
 *
 * page callback for recent nodes
 */
function iosdrupal_recent_nodes() {
  global $base_url;
  $iosdrupal_path = $base_url . '/' . drupal_get_path('module', 'iosdrupal');
  global $pager_total, $pager_page_array, $pager_limits;
  $perpage = variable_get('default_nodes_main', 50);
  if (isset($_GET['perpage']) && $_GET['perpage'] > 0) {
    $perpage = $_GET['perpage'];
  }
  $secure_key = variable_get('iosdrupal_key', '');
  $iosdrupal_type_setting = variable_get('iosdrupal_content_type', 'article');
  $json = (object) array('stat' => 'fail');
  $nodes = array();
  $nids = db_select('node', 'n')->extend('PagerDefault')
                  //->join('field_data_field_image', 'i', 'i.entity_id = n.nid')
                  ->fields('n', array('nid'))
                  //->fields('i', array('entity_id'))
                  ->condition('type', $iosdrupal_type_setting)
                  ->condition('status', 1)
                  ->orderBy('sticky', 'DESC')
                  ->orderBy('created', 'DESC')
                  ->limit($perpage)
                  ->addTag('node_access')
                  ->execute()
                  ->fetchCol();


  if ($nids) {
    $nodes = node_load_multiple($nids);
  }
  $new_nodes = array();

  if (!empty($nodes)) {
    $i = 0;
    foreach ($nodes as $key => $val) {
      $new_nodes[$i]->title = $val->title;

      $new_nodes[$i]->has_image = 0;
      $new_nodes[$i]->image_url = $iosdrupal_path . '/no_image.png';
      $new_nodes[$i]->large_image_url = $iosdrupal_path . '/no_image.png';
      if (isset($val->field_image['und'][0]['uri'])) {

        $uri = $val->field_image['und'][0]['uri'];
        $new_nodes[$i]->has_image = 1;
        $new_nodes[$i]->image_url = image_style_url(variable_get('iosdrupal_image_small', 'thumbnail'), $val->field_image['und'][0]['uri']); //file_create_url($uri);
        $new_nodes[$i]->large_image_url = image_style_url(variable_get('iosdrupal_image_large', 'large'), $val->field_image['und'][0]['uri']);
      }
      $i++;
    }


    $json->stat = 'ok';
    $json->nodes->page = $pager_page_array[0];
    $json->nodes->pages = $pager_total[0];
    $json->nodes->total = count($nids);
    $json->nodes->perpage = $pager_limits[0];
    $json->nodes->node = $new_nodes;
  }

  drupal_json_output($json);
}

?>
